Ansible Master for LET
=========

Prepare a host to be Ansible master for LET systems.

Role Variables
--------------

- `ansible_master_ansible_user`: defaults to `root`, an alternative user may be specified.
- `ansible_master_playbook_root`: directory in which playbooks and inventory file will be symlinked from the role. Default `master_playbook` in the home directory.
- `ansible_master_snipeit_apikey`: API key for access to the SnipeIT server.

Additionally, the special variable `ansible_master_force_update` can be set. This will trigger a complete update of all Ansible collections.
Do not include this in a standard play, as it is not idempotent.

Example:
```
ansible-playbook -e "ansible_master_force_update=true" -l kerbin playbooks/pb_linux_ansible_master.yml
```


Customization
----------------

Remove the symlinks in `ansible_master_playbook_root` and copy the corresponding files as a replacement. Subsequent Ansible runs will not overwrite the files.

