# Playbooks for System Configuration at LET

The playbooks have been split up to 

- improve readability
- speed up execution if only parts have to be reconfigured

Each service has its dedicated playbook. This playbook is imported by a higher level playbook,
on top of all playbooks is `site.yml`.

![Dependencies of playbooks and collections](docs/ansible_playbooks.png)

Execution of `site.yml` without a `--limits` parameter would configure all hosts with all roles
assigned, while `pb_linux_common.yml` would apply the basic standard configuration for all
hosts which are member of the *base_settings* group.

If a service configuration has changed, the corresponding playbook can be used to avoid
going through the complete base configuration. Without limits on the command line, all
hosts assigned to this service would be affected. I.e. applying `pb_linux_svc_goemaxima.yml`
would

- only configure hosts in the *goemaxima* group
- only apply the *goemaxima* configuration

The complete configuration run takes time, using a dedicated playbook will speed up the
process significantly.

## Installation

### Automated
**This requires an already configured Ansible master!** If the target system is managed in `SnipeIT`, set the Ansible attribute `ansible_master`.

Apply the playbook `playbooks/pb_linux_ansible_master.yml`, it installs the latest Ansible from
PyPi and all collections. If excecuted on a host already configured by this playbook as an Ansible master, most defaults for Ansible user, vault password and inventory file are already set. Simply run
```
ansible-playbook -l new_master_hostname site.yml
```
A more verbatim command which skips the basic configuration may look like
```
ansible-playbook -u root -l new_master_hostname -i inventory.yml  --vault-password-file ~/.ansible/vault_let.txt playbooks/pb_linux_ansible_master.yml
```

Standard execution of the playbook does not upgrades any collections installed. Set the variable `ansible_master_force_update` to force all dependencies to updated (not idempotent).

### Manual
Sensitive data has been encrypted with _Ansible Vault_. A host configured by Ansible already has the vault password installed. Otherwise get the Ansible vault key from Keepass, write it to a file (mode 600!), and set an environment variable `ANSIBLE_VAULT_PASSWORD_FILE` pointing to the file.

Manual installation is required if
- no Ansible master yet
- host is not in SnipeIT (but you may create your own inventory to use the playbook)
- you want to learn how everything is built

Ansible >= 2.11 is required! You may need to install with `pip`, because the version available
as OS package is usually older. For RHEL8 derivates, you also may have to install `pip` from _PyPi_, because it cannot always
install the ansible package, depending on the exact release:

```
dnf install git
pip3 install -I pip    # try without this first
pip3 install ansible
```

You also need the `snipeit` Python package for the inventory plugin, and `netaddr` for some address arithmetics:

```pip3 install snipeit netaddr```

This repository hosts the playbooks, not the modules doing the actual work. Required 
dependencies will be installed automatically when this collection is installed:

```ansible-galaxy collection install git+https://gitlab.ethz.ch/ansible-let/master_playbook```

To update, use the force switch `-f`, and to update all collections, use 

```ansible-galaxy collection install --force-with-deps git+https://gitlab.ethz.ch/ansible-let/master_playbook```

The collection will be installed in `~/.ansible/collections/ansible_collections/let/master_playbook`. For easy access, make a directory like
`master_playbook` somewhere, and link the following files/directories into the directory:

```
ln -s ~/.ansible/collections/ansible_collections/let/master_playbook/ansible.cfg .
ln -s ~/.ansible/collections/ansible_collections/let/master_playbook/inventory.yml .
ln -s ~/.ansible/collections/ansible_collections/let/master_playbook/site.yml .
ln -s ~/.ansible/collections/ansible_collections/let/master_playbook/playbooks .
```
Replace a links with a copy of the source file, if you want to customize the file.

## Inventory

All inventory information is pulled from SnipeIT. To gain access, you have to
configure your API key first. Read the `inventory.yml` for information how to
store and access the key with Ansible.

As a test, the inventory may be queried manually with

```
ansible-inventory --list -i inventory.yml
```

Last task is to get a private SSH key on the machine (access mode 600), which allows access to the systems to be managed. See _Keepass_ for the default Ansible SSH key.
